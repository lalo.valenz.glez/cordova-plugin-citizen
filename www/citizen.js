var cordova = require('cordova');

module.exports = {
    print: function(data, successCalback, errorCallback) {
        cordova.exec(successCalback, errorCallback, "Printer", "print", [data]);
    }
}